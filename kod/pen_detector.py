import cv2 as cv
import numpy as np


def pen_detect(image, hsv_min=(100, 100, 100), hsv_max=(120, 255, 255)):
    hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
    mask = cv.inRange(hsv, hsv_min, hsv_max)
    mask = cv.erode(mask, (3, 3), iterations=14)
    mask = cv.dilate(mask, None, iterations=2)
    return mask


cap = cv.VideoCapture(0, cv.CAP_DSHOW)
cv.waitKey(0)
ret, img = cap.read()
cv.imshow("Base image", img)
cap.release()
img = pen_detect(img)
cv.waitKey(1)
cv.destroyAllWindows()
