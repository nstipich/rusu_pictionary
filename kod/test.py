import numpy as np
import matplotlib.pyplot as plt
from PIL import Image as Im
import cv2 as cv
import model as mdl
import tensorflow as tf
import pen_detector as pd

ix, iy = 0, 0
draw = True
new_line = False
class_names = ['apple', 'banana', 'cake', 'car', 'dog', 'eye', 'fish', 'flower', 'house', 'snail']


def on_mouse(event, x, y, flags, param):
    global ix, iy, draw, new_line
    if event == cv.EVENT_LBUTTONDOWN and draw is True:
        if ix == 0 and iy == 0 or new_line is True:
            (ix, iy) = x, y
            new_line = False
        cv.line(window, (x, y), (ix, iy), (255, 255, 255), 17)
        (ix, iy) = x, y
    if event == cv.EVENT_RBUTTONDOWN:
        if draw is True:
            new_line = True
        draw = not draw


window = np.zeros((480, 480), np.uint8)

cv.namedWindow("Draw")
cv.setMouseCallback("Draw", on_mouse)

while True:
    cv.imshow("Draw", window)
    if cv.waitKey(1) & 0xFF == ord('q'):
        break
resized_window = cv.resize(window, (28, 28))
resized_window = resized_window/255
plt.imshow(Im.fromarray(resized_window))
image = resized_window.reshape((28, 28))
image = (np.expand_dims(image, -1))
image = (np.expand_dims(image, 0))

bitmap_image = cv.resize(window, (28, 28))
plt.imshow(Im.fromarray(bitmap_image))

model = tf.keras.models.load_model('models/test')

predictions = model(image)
if np.max(predictions) < 12:
    print("Nothing detected.")
else:
    print(
        "This image most likely belongs to {} with a {:.2f} percent confidence."
        .format(class_names[np.argmax(predictions)], np.max(predictions))
    )
cv.destroyAllWindows()
