import numpy as np
import matplotlib.pyplot as plt
from PIL import Image as Im
import cv2 as cv


def get_data():
    class_names = ['apple', 'banana', 'cake', 'car', 'dog', 'eye', 'fish', 'flower', 'house', 'snail']
    temp_train_x = []
    temp_train_y = []
    temp_val_x = []
    temp_val_y = []
    temp_test_x = []
    temp_test_y = []
    for class_num, classes in enumerate(class_names):
        data = np.load('data/' + classes + '.npy')
        for idx, value in enumerate(data[:14000]):
            if 10000 <= idx < 12000:
                image = value.reshape((28, 28, 1))
                temp_val_x.append(image)
                temp_val_y.append(class_num)
            elif idx >= 12000:
                image = value.reshape((28, 28, 1))
                temp_test_x.append(image)
                temp_test_y.append(class_num)
            else:
                image = value.reshape((28, 28, 1))
                temp_train_x.append(image)
                temp_train_y.append(class_num)

    train_data_x = np.array(temp_train_x)
    train_data_y = np.array(temp_train_y)
    val_data_x = np.array(temp_val_x)
    val_data_y = np.array(temp_val_y)
    test_data_x = np.array(temp_test_x)
    test_data_y = np.array(temp_test_y)

    train_data_x = train_data_x / 255
    val_data_x = val_data_x / 255
    test_data_x = test_data_x / 255

    train_data_x = train_data_x.reshape(100000, 28, 28, 1)
    val_data_x = val_data_x.reshape(20000, 28, 28, 1)
    test_data_x = test_data_x.reshape(20000, 28, 28, 1)
    return train_data_x, train_data_y, val_data_x, val_data_y, test_data_x, test_data_y

