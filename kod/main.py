import numpy as np
import matplotlib.pyplot as plt
from PIL import Image as Im
import cv2 as cv
import model as mdl
import pen_detector as pd
import tensorflow as tf
import random


def to_bitmap(image):
    bitmap_image = cv.resize(image, (28, 28))
    #plt.imshow(Im.fromarray(bitmap_image))
    bitmap_image = bitmap_image / 255
    bitmap_image = bitmap_image.reshape((28, 28))
    bitmap_image = (np.expand_dims(bitmap_image, -1))
    bitmap_image = (np.expand_dims(bitmap_image, 0))
    return bitmap_image


def predict(image, train):
    if not train:
        model = tf.keras.models.load_model('models/test')
    elif train:
        model = mdl.get_model()
    values = model(image)
    tf.keras
    if np.max(values) < 6:
        return "nothing"
    return class_names[np.argmax(values)]


def predict_loop():
    bitmap = to_bitmap(window)
    output_text = predict(bitmap, False)
    if output_text != guess_object:
        output_text = "PC guessed " + output_text + ", you lose!"
    else:
        output_text = "PC guessed " + output_text + " correctly. You win."
    return output_text


class_names = ['apple', 'banana', 'cake', 'car', 'dog', 'eye', 'fish', 'flower', 'house', 'snail']

window = np.zeros((480, 480), np.uint8)
cap = cv.VideoCapture(0, cv.CAP_DSHOW)
output_text = None
guess_object = None
while True:
    ret, img = cap.read()
    img = cv.flip(img, flipCode=1)
    mask = pd.pen_detect(img)
    mask = mask[:, 80:560]
    mask = cv.blur(mask, (3, 3))
    window = cv.bitwise_or(window, mask)
    img = img[:, 80:560]
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    output = cv.addWeighted(img, 1.0, window, 0.4, 0.0)
    if output_text:
        cv.putText(output, output_text, (0, 100), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1, cv.LINE_AA)
    if guess_object:
        cv.putText(output, "Draw: "+guess_object, (0, 25), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1, cv.LINE_AA)
    cv.imshow("Drawing", output)
    key = cv.waitKey(11) & 0xFF
    if key == 32:
        window = 0
        output_text = None
        guess_object = None
    elif key == ord('q'):
        break
    elif key == ord('s'):
        if not guess_object:
            output_text = "Press n to generate object to draw."
        else:
            output_text = predict_loop()
    elif key == ord('n'):
        guess_object = class_names[random.randint(0, 9)]
        output_text = None
cap.release()
cv.destroyAllWindows()
