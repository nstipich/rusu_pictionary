import tensorflow as tf
import load_bitmap
from tensorflow.keras import layers, models
import matplotlib.pyplot as plt


def get_model():
    train_data_x, train_data_y, val_data_x, val_data_y, test_data_x, test_data_y = load_bitmap.get_data()

    model = models.Sequential()
    model.add(layers.Conv2D(16, (3, 3), activation='relu', input_shape=(28, 28, 1)))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(32, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))

    model.add(layers.Flatten())
    model.add(layers.Dense(32, activation='relu'))
    model.add(layers.Dense(10))

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

    history = model.fit(train_data_x, train_data_y, epochs=5,
                        validation_data=(val_data_x, val_data_y))
    model.save('models/test')
    return model, history


def main():
    train_data_x, train_data_y, val_data_x, val_data_y, test_data_x, test_data_y = load_bitmap.get_data()
    model, history = get_model()
    test_loss, test_acc = model.evaluate(test_data_x, test_data_y, verbose=2)
    plt.plot(history.history['accuracy'], label='accuracy')
    plt.plot(history.history['val_accuracy'], label='val_accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim([0.5, 1])
    plt.legend(loc='lower right')

    model.summary()


if __name__ == '__main__':
    main()
